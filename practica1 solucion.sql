﻿USE practica1; -- selecciono la base de datos con la que voy a trabajar

/* 
  CONSULTA 1 
*/

SELECT e.emp_no,
       e.apellido,
       e.oficio,
       e.dir,
       e.fecha_alt,
       e.salario,
       e.comision,
       e.dept_no FROM emple e;

/*
    consulta 2
*/

SELECT 
  d.dept_no,
  d.dnombre,
  d.loc 
FROM 
  depart d;

/* 
  consulta 3 
*/

  SELECT DISTINCT
   e.apellido, e.oficio   
  FROM 
    emple e;

/* 
  consulta 4 
*/

  SELECT 
    d.dept_no, d.loc
  FROM 
    depart d;

/* 
  consulta 5
*/
  SELECT 
    d.dept_no,
    d.dnombre,
    d.loc 
  FROM 
    depart d;
/* 
  consulta 6
*/
  SELECT 
    COUNT(*) numeroEmpleados 
  FROM 
    emple e;








